const initialState={
    todoArray:{},
    counter:0
}

export const todoReducer=(state=initialState,action)=>
{
    switch(action.type)
    {
        case "ADD_TO_LIST":{
            let tempCounter=state.counter.valueOf();
            let temp={...state.todoArray};
            temp[tempCounter++]=action.payload;
            return{...state,todoArray:temp,counter:tempCounter}
        }
        case "REMOVE_FROM_LIST":{
            let temp={...state.todoArray};
            delete temp[action.payload]
            return{...state,todoArray:temp}
        }
        default:
                return state;
    }
}   