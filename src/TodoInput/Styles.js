import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
    container: {
      padding: 20,
      display : "flex",
      flexDirection :"row",
      justifyContent :"space-around",
    },
    button: {
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 20,
      borderRadius :10
    },
    inputBox : {
        borderColor : "#DDDDDD",
        borderWidth :2,
        borderRadius :10,
        width : "70%"
    },
  });