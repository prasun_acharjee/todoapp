import React from "react";
import {View,Text,TextInput,TouchableHighlight,Keyboard} from 'react-native';
import {requestAddTodo} from "../../Actions/actions";
import {connect} from "react-redux";
import { styles } from "./Styles";
import { bindActionCreators} from "redux";
  class TodoInput extends React.Component{
    constructor(props){
      super(props)
      this.state={todoItem:''}
    }

    click=()=>
    {    
      this.state.todoItem.trim();
      if(!this.state.todoItem.replace(/\s/g, '').length)
      {
        return;
      }
      
      const data={'text':this.state.todoItem};
      this.props.requestAddTodo(data);
      this.setState({todoItem:''});
      Keyboard.dismiss();
    }

    render(){
    return(
        <View style={styles.container}>
          
            <TextInput
             style={styles.inputBox}
             value={this.state.todoItem}
             onChangeText={(text)=>this.setState({todoItem:text})} 
             testID="InputField"
             />

            <TouchableHighlight 
            style={styles.button} 
            onPress={this.click}
            testID="Enter"
            >

              <Text>Enter</Text>

            </TouchableHighlight>

        </View>
    )
    }
  }
  const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({
        requestAddTodo
    },dispatch)
}


  export default connect(null,mapDispatchToProps)(TodoInput);