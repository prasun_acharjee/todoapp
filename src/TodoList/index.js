import React from "react";
import {FlatList} from 'react-native';
import TodoItem from "../TodoItem/index";
import {connect} from "react-redux";
import { selectItems } from "../../Selectors";
class TodoList extends React.Component{
    renderItem=({item})=>{
        return <TodoItem todo={item}/>
    }
    render(){
    return(
        <FlatList 
        data={this.props.todoArray} 
        renderItem={
            this.renderItem
            }
            keyExtractor={(item, index) => item+index}
        />
    )
    }
}
const mapStateToProps=(store)=>{
    return({
        todoArray:selectItems(store),
    })
}
export default connect(mapStateToProps,null)(TodoList);