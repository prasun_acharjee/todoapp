import React from "react";
import TodoInput from "./TodoInput/index";
import TodoList from './TodoList/index';
  class Home extends React.Component { 
  render(){
    return (
      <>
        <TodoInput/>
        <TodoList/>
      </>      
    );
  }
}
  
  export default Home;
  