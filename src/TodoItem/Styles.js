import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      padding: 20,
      display : "flex",
      flexDirection :"row",
      justifyContent :"space-around",
      alignItems :"center",
      borderRadius : 25,
      width: "80%",
      alignSelf :"center",
      marginTop : 20,
      elevation :5,
      marginBottom:20
    },
    button: {
      alignItems: "center",
      backgroundColor: "#DDDDDD",
      padding: 15,
      borderRadius :10,
      flex:1
    },
    text :{
        flex :4,
        marginRight:10
    }
  });