import React from "react";
import {View,Text,TouchableOpacity} from 'react-native';
import { styles } from "./Styles";
import { requestRemoveTodo } from "../../Actions/actions";
import {connect} from "react-redux";
import { bindActionCreators} from "redux";
class TodoItem extends React.Component{
    removeItem=()=>{
        this.props.requestRemoveTodo(this.props.todo.id)
    }
    render(){
    return(
        <View style={styles.container}>
            <Text style={styles.text} testID={`todoText${this.props.todo.id}`}>{this.props.todo.text}</Text>
            
            <TouchableOpacity 
            style={styles.button} 
            onPress={this.removeItem}
            testID={`deleteButton${this.props.todo.id}`}
            >
            
            <Text>Delete</Text>
            
            </TouchableOpacity>
        
        </View>
    )
    }
}
const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({
        requestRemoveTodo
    },dispatch)
}
export default connect(null,mapDispatchToProps)(TodoItem);