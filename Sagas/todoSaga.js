import {put,takeEvery,takeLatest,all} from "redux-saga/effects";
import {addTodo,removeTodo} from "../Actions/actions";
export function* todoSaga(action) {
  yield takeLatest ('ADD',addToList);
  yield takeLatest ('REMOVE',removeFromList);
}

function* addToList(action) { 
  yield put(addTodo(action.payload));
}
function* removeFromList(action) {
    yield put(removeTodo(action.payload));
}

