import { createSelector } from 'reselect';

const todoItems = state => state.todoReducer.todoArray;

export const selectItems = createSelector(
    todoItems,
    (todoArray) => {
        const ids = Object.keys(todoArray);
        return ids.map(id => ({id:id,text:todoArray[id].text}));
    }
);