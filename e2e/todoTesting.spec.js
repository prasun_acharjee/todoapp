describe("todoapp testing", () => {
  
  let inputButton=null;
  let inputField=null;

  beforeEach(async()=>{
    await waitFor(element(by.id('Enter'))).toBeVisible().withTimeout(10000);
    await waitFor(element(by.id('InputField'))).toBeVisible().withTimeout(10000);
    inputButton=element(by.id('Enter'));
    inputField=element(by.id('InputField'));
    await element(by.id('InputField')).clearText();
  })

  it("checking visibility of input field and button", async ()=> {
    await expect(element(by.id("Enter"))).toBeVisible();
    await expect(element(by.id("InputField"))).toBeVisible();   
  });
    
  it("testing proper input field",async ()=>{
    await inputField.replaceText("work");
    await expect(inputField).toHaveText("work");
  })
    
  it("empty input field not working",async ()=>{  
    let todoText0=element(by.id("todoText0"));
    await inputButton.tap();
    await expect(todoText0).toNotExist();
  })
    
  it("entering blank spaces does not create tasks",async ()=>{
    let todoText0=element(by.id("todoText0"));
    await inputField.replaceText("  ");  
    await inputButton.tap();
    await expect(todoText0).toNotExist();
  })
    
  it("adding task 1 to the list",async ()=>{
    await inputField.replaceText("task1");
    await inputButton.tap();
    await waitFor(element(by.id('todoText0'))).toBeVisible().withTimeout(10000);
    await expect(element(by.id("todoText0"))).toHaveText("task1");
    })
    
  it("adding task 2 to the list",async ()=>{
    await inputField.replaceText("task2");
    await inputButton.tap();
    await waitFor(element(by.id('todoText1'))).toBeVisible().withTimeout(10000);
    await expect(element(by.id("todoText1"))).toHaveText("task2");
    })

  it("deleting the task 1",async ()=>{
    let deleteButton0=element(by.id("deleteButton0"));
    let todoText0=element(by.id("todoText0"));
    await waitFor(deleteButton0).toBeVisible().withTimeout(10000);
    await deleteButton0.tap();
    await waitFor(todoText0).toNotExist().withTimeout(10000);
    await expect(todoText0).toNotExist();
    })

});
