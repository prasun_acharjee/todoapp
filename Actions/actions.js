export const requestAddTodo =(data)=>{
    return({type:'ADD',payload:data})
}
export const requestRemoveTodo =(index)=>{
    return({type:"REMOVE",payload:index})
}

export const addTodo=(data)=>{
    return({type:'ADD_TO_LIST',payload:data})
}

export const removeTodo=(index)=>{
    return({type:'REMOVE_FROM_LIST',payload:index})
}

