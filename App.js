/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {Provider} from "react-redux";
import Home from './src/Home';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import {rootReducer} from "./Reducers/Index";
import {todoSaga} from "./Sagas/todoSaga";
const saga = createSagaMiddleware();

const Store =()=> createStore(rootReducer,applyMiddleware(saga,logger));
const store=Store();
saga.run(todoSaga);

class App extends React.Component{
  render(){
  return (
    <Provider store={store}>
      <Home/>
    </Provider>
    
  );
  }
};

export default App;
